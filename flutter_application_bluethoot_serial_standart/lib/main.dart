import 'dart:ffi';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import './joypad.dart';

Future main() async {
  /*
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]);

  // disable all UI overlays (show fullscreen)
  await SystemChrome.setEnabledSystemUIOverlays([]);
  */
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft])
      .then((_) {
    runApp(MaterialApp(home: RobotApp()));
  });
}

class RobotApp extends StatefulWidget {
  late BluetoothConnection connection;
  late FlutterBluetoothSerial bluetooth;
  List<BluetoothDevice> _devicesList = [];
  BluetoothDevice? _device;
  bool _connected = false;
  bool _pressed = false;

  var _pwmR = 0;
  var _pwmL = 0;
  var _pwmRD = 0;
  var _pwmLD = 0;
  var _preData = "";

  @override
  _RobotApp createState() => _RobotApp();
}

class _RobotApp extends State<RobotApp> {
  late TextEditingController _controller;

  /* Variables envio datos algoritmo */
  var data = "";
  var cont_send = 0;
  var flag_run = false;
  var flag_back = false;
  var flag_stop = false;

  @override
  void initState() {
    super.initState();

    widget.bluetooth = FlutterBluetoothSerial.instance;
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> bluetoothConnectionState(
      FlutterBluetoothSerial obj, List<BluetoothDevice> lista) async {
    try {
      lista = await obj.getBondedDevices();
      print("LISTA DEVICES: ${lista[0].address}");

      BluetoothConnection.toAddress(lista[0].address).then((conn) async {
        widget.connection = conn;
        widget.connection.output
            .add(Uint8List.fromList(utf8.encode("b" + "~")));

        widget.connection.input!.listen(_onDataRecibed);

        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Conectado!"),
              );
            });
      });
    } catch (err) {
      print("Error");
    }
  }

  void _sendJoyOff(Offset cordenadas) async {
    data = "";
    if (cordenadas.dy.round() < 0) {
      // Adelante
      flag_run = true;
      flag_back = false;
      flag_stop = false;

      // 0 hasta -30 donde -30 es igual 100% duty.
      /*
          -30 --> 100%
          dy --> x

          dy*100%/-30

        */
      var op = (cordenadas.dy.round() * 100) / -30;
      if (op.round() < 45) {
        // Restamos el valor de duty que determina el Joy R al querer girar.
        widget._pwmL = 45;
        widget._pwmR = 45;
      } else {
        widget._pwmL = op.round();
        widget._pwmR = op.round();
      }

      data = "R";

      if (cont_send == 0) {
        print("[Joy Send] data: R");
        _sendMessage("R");
        cont_send == 1;
      }
    } else if (cordenadas.dy.round() > 0) {
      // Atras
      flag_run = false;
      flag_back = true;
      flag_stop = false;

      data = "B";

      widget._preData = data;
      print("[Joy Send] data: B");
      _sendMessage(data);
      //await Future.delayed(Duration(milliseconds: 500));

    } else if (cordenadas.dy.round() == 0) {
      // Stop
      flag_run = false;
      flag_back = false;
      flag_stop = true;

      widget._pwmR = 45;
      widget._pwmL = 45;

      data = "S";
      cont_send = 0;

      print("[Joy Send] data: ${data}");
      _sendMessage(data);
    }
  }

  // Enviar mensaje desde input.
  void _inputSend() async {
    print("Campo de texto: ${_controller.text.trim()}");
    _sendMessage(_controller.text.trim());
  }

  // CallBack data recibed.
  void _onDataRecibed(Uint8List data) async {
    //String _decode = String.fromCharCodes(data);
    print("Data recibida: ${data}");

    // [13, 10, 99, 111, 108, 111, 109, 98, 105, 97, 50, 48]
  }

  // Function to send sms to device connected.
  void _sendMessage(String sms) async {
    widget.connection.output.add(Uint8List.fromList(utf8.encode(sms)));
  }

  // Connect HC with funcion async bluetoothConnectionState()
  void _connectHC() async {
    bluetoothConnectionState(widget.bluetooth, widget._devicesList);
  }

/*
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(TextField(
          controller: _controller,

        child:           
          onSubmitted: (String value) async {
            await showDialog<void>(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Thanks!'),
                  content: Text(
                      'You typed "$value", which has length ${value.characters.length}.'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('OK'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      ),
    );
  }
*/

  Widget build(BuildContext context) {
    // Scaffold es un layout para la mayoría de los Material Components.
    return Scaffold(
      backgroundColor: Colors.black,
      // el body es la mayor parte de la pantalla.
      body: Center(
        child: Container(
          color: Colors.grey,
          margin: const EdgeInsets.all(2.0),
          padding: const EdgeInsets.all(14.0),
          child: Column(children: [
            Spacer(),
            IconButton(
                color: Colors.black,
                iconSize: 50,
                onPressed: _connectHC,
                icon: const Icon(Icons.bluetooth_drive)),
            Spacer(),
            TextField(
              decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0)),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0))),
              controller: _controller,
              cursorColor: Colors.black,
            ),
            IconButton(
              icon: Icon(Icons.send_sharp),
              tooltip: 'Send message',
              onPressed: _inputSend,
            ),
            Spacer(),
            Row(
              children: [
                SizedBox(width: 48),
                Joypad(
                  /* Joy Left */
                  onChange: (Offset delta) => _sendJoyOff(delta),
                ),
                Spacer(),
                Joypad(
                  /* Joy Right */

                  onChange: (Offset delta) async {
                    //print("Joy PWM ${delta.dx.round()}");

                    // deecha.
                    if (delta.dx.round() > 0) {
                      /*
                          30 --> maxPWMduty - 45(MinPWM)   
                          x --> x

                          60 --> 100%

                         Se realiza regla de 3 suponiendo que el valor maximo cuando sea 30, es la diferencia
                         entre el maxduty del momento determinado por el Joy Left
                       */
                      /*
                      widget._pwmRD =
                          ((delta.dx.round() * (widget._pwmR - 0)) / 30)
                              .round();
                      */
                      print("[Joy Send] data: D");
                      _sendMessage("D");
                    }
                    // Izquierda
                    else if (delta.dx.round() < 0) {
                      /*
                      widget._pwmLD =
                          (((delta.dx.round() * -1) * (widget._pwmL - 0)) / 30)
                              .round();
                      */
                      print("[Joy Send] data: I");
                      _sendMessage("I");
                    }

                    // center

                    else if (delta.dx.round() == 0) {
                      print("[Joy Send] data: C");
                      _sendMessage("C");
                    }
                  },
                ),
                SizedBox(width: 48),
              ],
            ),
            SizedBox(height: 24),
          ]),
          /*
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center, // x
                crossAxisAlignment: CrossAxisAlignment.center, // y
                children: <Widget>[
                  TextField(
                    decoration: const InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.0)),
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.0))),
                    controller: _controller,
                    cursorColor: Colors.black,
                  ),
                  IconButton(
                    icon: Icon(Icons.send_sharp),
                    tooltip: 'Send message',
                    onPressed: _inputSend,
                  )
                ])*/
        ),
      ),
      /*
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        tooltip:
            'Connect', // utilizado por las tecnologías de accesibilidad para discapacitados
        child: const Icon(Icons.bluetooth_drive),
        onPressed: _connectHC,
      ),*/
    );
  }
}

import 'dart:ffi';
import 'dart:convert' show utf8;
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

import 'dart:async';
import 'dart:convert';

late BluetoothCharacteristic example;
bool isRead = false;
Map valueMap = {};
var x;
var y;
var z;

void main() {
  runApp(MaterialApp(home: TutorialHome()));
}

class TutorialHome extends StatefulWidget {
  List<String> names = <String>[];
  List<BluetoothDevice> devicesList = <BluetoothDevice>[];
  List<BluetoothCharacteristic> _charc = <BluetoothCharacteristic>[];

  /* Datos ESP32 */
  String _mac_device_esp = "84:CC:A8:2C:7E:4A";
  String _serviceUART = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
  String _uartTX = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";
  String _uartRX = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";

  late FlutterBlue flutterBlue;
  late BluetoothDevice _connection;

  late List<int> _data;
  late BluetoothCharacteristic chaWrite;
  late BluetoothCharacteristic chaRead;
  late BluetoothCharacteristic chaNotify;

  @override
  _HubApp createState() => _HubApp();
}

class PageAMR extends StatefulWidget {
  BluetoothCharacteristic streamRead;

  PageAMR(this.streamRead);

  @override
  _PageAMR createState() => _PageAMR();
}

class _HubApp extends State<TutorialHome> {
  void _readBLE() async {
    //await widget.chaWrite
    //    .write(utf8.encode("Saludos terricola!"), withoutResponse: true);
    //await widget.chaRead.read();
    //print(data);
  }

  @override
  void initState() {
    super.initState();
    widget.flutterBlue = FlutterBlue.instance;

    widget.names = <String>[];
    widget.devicesList = <BluetoothDevice>[];

    // Listen to scan results
    widget.flutterBlue.scanResults.listen((results) {
      // do something with scan results
      for (ScanResult r in results) {
        if (!widget.devicesList.contains(r.device)) {
          widget.devicesList.add(r.device);
          /*
          if (r.device.id.id == widget._mac_device_esp) {
            // ignore: avoid_print
            print("Yes encontado!: ${r.device.id.id} ");
          }
          */
        }
      }
    });

    // Start scanning
    widget.flutterBlue.startScan(timeout: Duration(seconds: 4));
    // Stop scanning
    widget.flutterBlue.stopScan();
  }

  void _repeatScanBLE() {
    // Start scanning
    widget.flutterBlue.startScan(timeout: Duration(seconds: 4));

    // Listen to scan results
    widget.flutterBlue.scanResults.listen((results) {
      // do something with scan results
      for (ScanResult r in results) {
        if (!widget.devicesList.contains(r.device)) {
          widget.devicesList.add(r.device);
          /*
          if (r.device.id.id == widget._mac_device_esp) {
            // ignore: avoid_print
            print("Yes encontado!: ${r.device.id.id} ");
          }
          */
        }
      }
    });
  }

  void _connectDevice(String adress) async {
    widget.devicesList.forEach((element) async {
      // despues scan veritifco si la direccion mac es la de la ESP32 y me conecto.
      if (element.id.id == adress) {
        element.connect().then((value) async {
          widget._connection = element;
          print("[BLE] You already connect to ESP32_MAC: ${element.id.id}");

          widget._connection.discoverServices().then((value) async {
            value.forEach((service) async {
              if (service.uuid.toString() == widget._serviceUART) {
                //print("[BLE] ${service.characteristics}");
                service.characteristics.forEach((char) async {
                  print("[BLE] CHAR ESP32: ${char}");
                  if (char.uuid.toString() == widget._uartRX) {
                    // usado para escribir datos TX (REDMI) RX(RX_ESP32).
                    widget.chaWrite = char;
                  }
                  if (char.uuid.toString() == widget._uartTX) {
                    // usado para escribir datos RX (REDMI) TX(RX_ESP32).
                    widget.chaRead = char;
                    widget.chaRead.setNotifyValue(true).then((value) async {
                      widget.chaRead.value.listen((data) async {
                        isRead = false;
                        //print("[BLE-READ] Data: ${String.fromCharCodes(data)}");

                        valueMap = json.decode(String.fromCharCodes(data));
                        print(valueMap);
                      });
                    });
                  }
                });
              }
/*
              //print("[BLE] Services are: ${service}\n");
              service.characteristics.forEach((element) async {
                widget._charc.add(element);
//                print("[BLE] charcacteristcis is: ${element}");

                if (element.properties.write) {
                  //await element.write([0x48, 0x4F, 0x4C, 0x41]);
                  widget.chaWrite = element;
                  //List<int> reData = await element.read();
                }

                if (element.properties.read) {
                  widget.chaRead = element;
                }

                if (element.properties.notify) {
                  widget.chaNotify = element;
                  element.setNotifyValue(true).then((value) {
                    widget.chaNotify.value.listen((event) {
                      print(String.fromCharCodes(event));
                    });
                  });
                }
                
                if (element.deviceId.id == widget._mac_device_esp) {
                  element.write([0x12, 0x34]);
                }
                
              });*/
            });
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color.fromARGB(115, 10, 10, 10),
          tooltip:
              'Connect', // utilizado por las tecnologías de accesibilidad para discapacitados
          child: Icon(Icons.add),
          onPressed: () {
            _connectDevice(widget._mac_device_esp);
          },
        ),
        /*
        appBar: AppBar(
          backgroundColor: Color.fromARGB(115, 15, 15, 15),
          leading: IconButton(
            icon: Icon(Icons.menu),
            tooltip: 'MAM - CARS',
            onPressed: _test,
          ),
          title: const Text('MAM - CARS'),
        ),*/
        // el body es la mayor parte de la pantalla.
        body: Center(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    color: Colors.black12,
                    height: 130.0,
                    width: 130.0,
                    margin: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center, // x
                      crossAxisAlignment: CrossAxisAlignment.center, // y
                      children: <Widget>[
                        IconButton(
                          iconSize: 80.0,
                          icon: Icon(Icons.bluetooth_sharp),
                          tooltip: 'hover message',
                          onPressed: () {
                            print("ESCANEANDO!");
                          },
                        ),
                        Text("Devices View")
                      ],
                    ),
                  ),
                  Container(
                      color: Colors.black12,
                      height: 130.0,
                      width: 130.0,
                      margin: const EdgeInsets.all(10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          IconButton(
                            iconSize: 80.0,
                            icon: Icon(Icons.cloud_circle_sharp),
                            tooltip: 'hover message',
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        PageAMR(widget.chaRead)),
                              );
                            },
                          ),
                          Text("AMR View")
                        ],
                      ))
                ]),
          ],
        )),

        //floatingActionButton: FloatingActionButton(
        //  backgroundColor: Color.fromARGB(115, 10, 10, 10),
        //  tooltip:
        //      'Add', // utilizado por las tecnologías de accesibilidad para discapacitados
        //  child: const Icon(Icons.add),
        //  onPressed: null,
        //),
      );
}

class _PageAMR extends State<PageAMR> {
  Text pepe = Text("");
  late Timer timer;
  @override
  void initState() {
    timer = Timer.periodic(const Duration(microseconds: 1), (s) async {
      if (isRead) {
      } else {
        isRead = true;
        readAMR();
        changeText();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void readAMR() async {
    await widget.streamRead.read();
  }

  void changeText() {
    setState(() {
      x = valueMap["x"];
      y = valueMap["y"];
      z = valueMap["z"];
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
            child: Container(
                alignment: Alignment.center,
                color: Color.fromARGB(31, 255, 203, 203),
                width: 300,
                height: 300,
                margin: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Text("x --> $x"),
                    Text("y --> $y"),
                    Text("z --> $z")
                  ],
                ))),
      );
}
